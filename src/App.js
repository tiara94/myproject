import React, {Component} from 'react';
import Header from "./components/Header";
import TodoList from "./components/TodoList";

export default class App extends Component {
  render = () => {
    return (
      <div>
        <Header />
        <TodoList />
      </div>
    );
  }
}